<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route for authentication
Auth::routes();

// Route for frontend
Route::get('/','IndexController@index')->name('index');
Route::get('/gioi-thieu', 'IndexController@about')->name('gioi-thieu');
//Route::get('/khuyen-mai', 'IndexController@sale')->name('khuyen-mai');

Route::get('/dang-ky-kham', 'IndexController@registerMedicalApplication')->name('dang-ky-kham');
Route::post('/dang-ky-kham', 'IndexController@sendRegisterMedicalApplication');
Route::get('/dang-ky-thanh-vien', 'IndexController@registerMember')->name('dang-ky-thanh-vien');
Route::get('/dang-ky-thanh-vien', 'IndexController@registerMember')->name('dang-ky-thanh-vien');
Route::get('/thong-tin-benh-nhan', 'IndexController@personalInformation')->name('thong-tin-benh-nhan');

Route::get('/bac-sy/dang-ky', 'IndexController@registerDoctor')->name('dang-ky-bac-sy');
Route::post('/bac-sy/dang-ky', 'IndexController@sendRegisterDoctor');
Route::get('/bac-sy/quy-dinh', 'IndexController@doctorPolicy')->name('quy-dinh-bac-sy');
Route::get('/bac-sy/danh-sach', 'IndexController@doctorList')->name('danh-sach-bac-sy');
//Route::get('/bac-sy/thong-tin', 'IndexController@doctorInfomation')->name('thong-tin-bac-sy');

Route::get('/san-pham', 'IndexController@services')->name('san-pham');
Route::get('/dung-cu-y-te', 'IndexController@medicalInstruments')->name('dung-cu-y-te');
Route::get('/thuoc', 'IndexController@medical')->name('thuoc');

Route::get('/tin-tuc', 'IndexController@news')->name('tin-tuc');
Route::get('/cam-nang', 'IndexController@handBook')->name('hand-book');
Route::get('/tuyen-dung', 'IndexController@recruit')->name('recruit');
Route::get('/khuyen-mai', 'IndexController@sale')->name('sale');

Route::get('/lien-he', 'IndexController@contact')->name('contact');




// Route for backend
// All below route go with 'admin' prefix and need authentication
// All route name with add admin. prefix
Route::group([	'prefix' => 'admin',	'middleware' => 'auth', 'as' => 'admin.'	], function() {

	Route::get('/','AdminController@index')
	->name('index');

    Route::group([ 'middleware' => ['permission:manage-user']], function() {
		Route::resource('user',	'UserController');
    });

    Route::group(['middleware' => ['permission:manage-role']], function() {
        Route::resource('role', 'RoleController');
    });

    Route::group(['middleware' => ['role:admin']], function() {
        Route::get('permission/install','PermissionController@install')->name('permission.install');
        Route::post('permission/install','PermissionController@installPost')->name('permission.generate');
		Route::resource('permission',	'PermissionController');
    });

    Route::group(['middleware' => ['permission:manage-category']], function() {
        Route::resource('category', 'CategoryController');
    });

    Route::group(['middleware' => ['permission:manage-tag']], function() {
        Route::resource('tag', 'TagController');
    });

    Route::group(['middleware' => ['permission:manage-post']], function() {
        Route::resource('post', 'PostController');
    });

}); //End route group admin

Route::get('post/{slug}', 'IndexController@viewPost');
