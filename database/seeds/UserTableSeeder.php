<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User::truncate();
        $role_user = Role::where('name','User')->first();
        $role_author = Role::where('name','Author')->first();
        $role_admin = Role::where('name','Admin')->first();

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('123456');
        $user->phone = '0974040092';
        $user->save();
        $user->roles()->attach($role_user);



        $author = new User();
        $author->name = 'Author';
        $author->email = 'author@gmail.com';
        $author->password = bcrypt('123456');
        $author->phone = '0974040093';
        $author->save();
        $author->roles()->attach($role_author);

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('123456');
        $admin->phone = '0974040094';
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
