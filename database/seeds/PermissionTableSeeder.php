<?php

use Illuminate\Database\Seeder;
use App\Permission;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();        
		DB::table('permissions')->delete();
		DB::table('roles')->delete();
		DB::table('users')->delete();

        $permission_user = new Permission();
        $permission_user->name = 'manage-user';
        $permission_user->display_name = 'Manage User';
        $permission_user->description = 'CRUD all users';
        $permission_user->save();

        $permission_post = new Permission();
        $permission_post->name = 'manage-post';
        $permission_post->display_name = 'Manage Post';
        $permission_post->description = 'CRUD all posts';
        $permission_post->save();

        // $faker = Faker\Factory::create();
        // foreach( range(1,1000) as $index ){
        //     Permission::create([
        //         'display_name' => $faker->name,
        //         'name' => str_slug($faker->name),
        //         'description' => $faker->paragraph(1),
        //     ]);
        // }        
    }
}
