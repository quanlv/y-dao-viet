<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Role::truncate();

        $manage_user = Permission::where('name','manage-user')->first();
        $manage_post = Permission::where('name','manage-post')->first();

        $role_user = new \App\Role();
        $role_user->name = 'user';
        $role_user->display_name = 'User';
        $role_user->description = 'Thành viên';
        $role_user->save();

        $role_author = new \App\Role();
        $role_author->name = 'author';
        $role_user->display_name = 'Author';
        $role_author->description = 'Tác giả';
        $role_author->save();        

        $role_admin = new \App\Role();
        $role_admin->name = 'admin';
        $role_user->display_name = 'Admin';
        $role_admin->description = 'Quản trị viên';
        $role_admin->save();

        $role_admin->attachPermissions(array(
        	$manage_user
        	));
        $role_admin->attachPermissions(array(
        	$manage_post
        	));
        $role_author->attachPermissions(array(
        	$manage_post
        	));
    }
}
