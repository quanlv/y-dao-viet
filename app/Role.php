<?php 

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
	protected $guarded = ['id'];
	protected $fillable = ['name','display_name','description'];
}