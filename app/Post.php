<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = ['id', 'category'];

    public function category(){
    	return $this->belongsToMany('\App\Category','post_category','post_id','category_id');
    }

    public function hasCategory($category)
    {
        if ($this->category()->where('category_id',$category)->first()){
            return true;
        }
        return false;
    }

    public function tag(){
    	return $this->belongsToMany('\App\Category','post_tag','post_id','tag_id');
    }

    public function hasTag($category)
    {
        if ($this->category()->where('tag _id',$category)->first()){
            return true;
        }
        return false;
    }

    public function author(){
    	return $this->belongsTo('\App\User','author_id');
    }

    public function getUrl(){
        $base_url = Url('/');
        $prefix_url = 'post';
        $url = $base_url.'/'.$prefix_url.'/'.$this->slug.'';
        return $url;
    }

    public function getShortDec(){
        if (strlen($this->description) > 100){
            return substr($this->description, 0,strpos($this->description, ' ', 100)) . ' ...';
        } else {
        return $this->description;
        }
    }

    public function getEditUrl(){
        return route('edit-post',['id'=>$this->id]);
    }

}
