<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
	public function index(){
        $categories = Category::all();        
        return view('admin.category.index',compact('categories'));
    }

    //Show role details
    //Method get
    public function show($category){
        // $category = Category::findOrFail($category);
        // return view('admin.category.show',compact('role'));
    }

    //Return create role page
    //Method get
    public function create(){
        // $categorys = Category::all();
        // $roles = Role::all();
        // return view('admin.category.create',compact('categorys','roles'));
    }

    //Store a role to database
    //Method post
    public function store(CategoryRequest $request){
        try {
            $category = Category::create([
                'name' => $request['name'],
                'slug' => $request['slug'],
                'description' => $request['description'],
                'parent_category_id' => $request['parent_category_id'],
                ]);
            \Session::flash('flash_message','Đã tạo người dùng '.$category->name .' thành công!');
            return redirect()->route('admin.category.index');
        } catch (Exception $e){
            return dd($e);
        }
        // try {
            
        //     $category =  Category::create([
        //     'name' => $request['name'],
        //     'email' => $request['email'],
        //     'password' => bcrypt($request['password']),
        //     ]);

        //     $roles = Role::all();
        //     foreach ($roles as $role) {
        //         echo $request->input($role->id).PHP_EOL;
        //         if($request->input($role->id)){
        //            $category->attachRole($role);
        //         }
        //     }
        //     //dd($request->all());
        //     //return 0;


        //     \Session::flash('flash_message','Đã tạo người dùng '.$category->name .' thành công!');
        //     return redirect()->route('admin.category.index');
        // } catch (\Illuminate\Database\QueryException $e) {
        //     if($e->errorInfo[1]==1062){
        //         return Redirect::back()->withErrors('Dữ liệu nhập trùng với dữ liệu trong cơ sở diệu');
        //     }
        //     return Redirect::back()->withErrors($e->errorInfo[2]);
        // }

    }

    //Show role edit page
    //Method get
    public function edit($category){
        // $category = Category::findOrFail($category);
        // $roles = Role::all();
        // return view('admin.category.edit',compact('category','roles'));
    }

    //Update a role
    //Method put patch
    public function update($category, CategoryRequest $request){
        
        // $category = Category::findOrFail($category);
        // $roles = Role::all();
        // $category->roles()->sync([]); 
        // foreach ($roles as $role) {
        //         //echo $request->input($role->id).PHP_EOL;
        //         if($request->input($role->id)){
        //            $category->attachRole($role);
        //         }
        //     }
        // if($request->has('password')){
        //     $category->password = bcrypt($request->input('password'));
        // }
        // //else return "Mật khẩu rỗng";
        // $category->name = $request->input('name');
        // $category->save();
        // \Session::flash('flash_message','Đã sửa người dùng thành công!');       

        // return Redirect::back();
    }
    
    //Remove a role
    //Method delete
    public function destroy($category){
        Category::whereId($category)->delete();
        \Session::flash('flash_message','Đã xóa danh mục thành côngs!');
        return redirect()->route('admin.category.index');

    }
}
