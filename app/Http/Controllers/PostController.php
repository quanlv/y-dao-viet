<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Illuminate\Support\Facades\Input; 
use Auth;

class PostController extends Controller
{
	public function index() {
		$posts = Post::paginate();
		return view('admin.post.index', compact('posts'));
	}

	public function create() {
		$categories = Category::all();
		return view('admin.post.create', compact('categories'));
	}

	public function store(Request $request) {

		
		$slug = str_slug($request->input('title'));
		// Assign default slug
		Input::merge(['author_id' => Auth::user()->id]);
		if( !$request->input('slug')) {
			Input::merge(['slug' => $slug]);
		}
		// Check for duplicate slug
		$suffix = 1;
		while(Post::where('slug','=',$request->input('slug'))->first()){
			Input::merge(['slug' => $slug.'-'.$suffix++]);
		}
		// Add post from request		
		try {
			$post = Post::create($request->all());
			if($request->input('category')) {				
				foreach ($request->category as $x){
					$post->category()->attach($x);	
				}
			}

			\Session::flash('flash_message','Đã tạo bài viết '.$post->title .' thành công!');
			return redirect()->route('admin.post.index');
			return redirect()->route('admin.post.edit', ['id' => $post->id]);
		} catch (\Exception $e) {
			return $e;
		}
	}

	public function edit(Post $post) {
		
		//return Post::find($post)->title;
		$categories = Category::all();
		return view('admin.post.edit', compact('post', 'categories'));
	}

	public function update(Post $post, Request $request) {
		$post->category()->detach();
		$post->update($request->all());
		if($request->input('category')) {				
			foreach ($request->category as $x){
				$post->category()->attach($x);	
			}
		}
		return redirect()->route('admin.post.edit', ['id' => $post->id]);
	}

	public function destroy(Post $post) {
		Post::destroy($post->id);
		return redirect()->route('admin.post.index');
	}
    
}
