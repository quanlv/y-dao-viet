<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use Redirect;
use Route;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PermissionRequest;

class PermissionController extends Controller
{
    //Return index, show list of permissions
    //Method get
    public function index(){
    	$permissions = Permission::paginate(20);

    	return view('admin.permission.index',compact('permissions'));
    }

    //Show permission details
    //Method get
    public function show($permission){
        $permission = Permission::findOrFail($permission);
    	return view('admin.permission.show',compact('permission'));
    }

	//Return create permission page
    //Method get
    public function create(){
    	$roles = Role::all();
    	return view('admin.permission.create',compact('roles'));
    }

    //Store a permission to database
    //Method post
    public function store(PermissionRequest $request){
        try {
            $permission =  Permission::create($request->all());
            \Session::flash('flash_message','Đã tạo quyền '.$permission->name .' thành công!');
            return redirect()->route('admin.permission.index');
        } catch (\Illuminate\Database\QueryException $e) {
            if($e->errorInfo[1]==1062){
                return Redirect::back()->withErrors('Dữ liệu nhập trùng với dữ liệu trong cơ sở diệu');
            }
            return Redirect::back()->withErrors($e->errorInfo[2]);
        }

    }

    //Show permission edit page
    //Method get
    public function edit($permission){
        $permission = Permission::findOrFail($permission);
    	return view('admin.permission.edit',compact('permission'));
    }

    //Update a permission
    //Method put patch
    public function update($permission, PermissionRequest $request){
        $permission = Permission::findOrFail($permission);
        $permission->update($request->all());
        \Session::flash('flash_message','Đã sửa quyền thành công!');
        return Redirect::back();
    }

    //Remove a permission
    //Method delete
    public function destroy($permission){
        $permission = Permission::findOrFail($permission);
        $permission->delete();
        \Session::flash('flash_message','Đã xóa quyền thành côngs!');
        return redirect()->route('admin.permission.index');

    }

    public function install(){
        return 0;
        $routes = Route::getRoutes(); //Khoi tao route collection
        $middlewares = array('admin'); //Khoi tao danh sach middleware

        foreach ($routes as $value) {
            if(is_array($value->action['middleware'])){ //Kiem tra neu middleware la array
                $middlewares = array_merge($middlewares,$value->action['middleware']); //Chen middleware array vao danh sach middleware
            }
        }

        $middlewares = array_unique($middlewares); //Loc cac ket qua trung lap
        $permissions = array();

        foreach($middlewares as $value){

            if (strpos($value,'permission:')===0){ //Kiem tra neu co permission trong middleware
                $value = substr($value,11);
                array_push($permissions,$value); //Dua permission vao danh sach
            }
        }

        //print_r($permissions); //In ra danh sach
        return view('admin.permission.install',compact('permissions'));
    }

    public function installPost(){
        return 0;
        $routes = Route::getRoutes(); //Khoi tao route collection
        $middlewares = array('admin'); //Khoi tao danh sach middleware

        foreach ($routes as $value) {
            if(is_array($value->action['middleware'])){ //Kiem tra neu middleware la array
                $middlewares = array_merge($middlewares,$value->action['middleware']); //Chen middleware array vao danh sach middleware
            }
        }

        $middlewares = array_unique($middlewares); //Loc cac ket qua trung lap
        $permissions = array();

        foreach($middlewares as $value){

            if (strpos($value,'permission:')===0){ //Kiem tra neu co permission trong middleware
                $value = substr($value,11);
                array_push($permissions,$value); //Dua permission vao danh sach
            }
        }

        DB::table('permission_role')->truncate();
        DB::table('permissions')->delete();

        foreach($permissions as $permission){
        $permission = Permission::create([
            'name' => $permission,
           ]);
        $role_admin = Role::where('name','admin')->first();
        $role_admin->attachPermissions(array(
            $permission
            ));
        }

        \Session::flash('flash_message','Đã khởi tạo quyền thành công!');
        return redirect()->route('admin.permission.index');
    }
}
