<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Route;

class RouteController extends Controller
{
    public function index(){
    	$routes = Route::getRoutes(); //Khoi tao route collection
    	$middlewares = array('admin'); //Khoi tao danh sach middleware

    	foreach ($routes as $value) {
 		   	if(is_array($value->action['middleware'])){ //Kiem tra neu middleware la array
    			$middlewares = array_merge($middlewares,$value->action['middleware']); //Chen middleware array vao danh sach middleware
    		}
		}
		
		$middlewares = array_unique($middlewares); //Loc cac ket qua trung lap
		$permissions = array();

		foreach($middlewares as $value){
			
			if (strpos($value,'permission:')===0){ //Kiem tra neu co permission trong middleware
				$value = substr($value,11);
				array_push($permissions,$value); //Dua permission vao danh sach
			}
		}

		print_r($permissions); //In ra danh sach

		return response('')
                ->header('Content-Type', 'text/plain'); //Set header as text plan instead of html 
                
    }

}
