<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Redirect;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    //Return index, show list of users
    //Method get
    public function index(){
        $roles = Role::all();
        $users = User::paginate(20);        
        return view('admin.user.index',compact('users','roles'));
    }

    //Show role details
    //Method get
    public function show($user){
        $user = User::findOrFail($user);
        return view('admin.user.show',compact('role'));
    }

    //Return create role page
    //Method get
    public function create(){
        $users = User::all();
        $roles = Role::all();
        return view('admin.user.create',compact('users','roles'));
    }

    //Store a role to database
    //Method post
    public function store(UserRequest $request){
        try {
            
            $user =  User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            ]);

            $roles = Role::all();
            foreach ($roles as $role) {
                echo $request->input($role->id).PHP_EOL;
                if($request->input($role->id)){
                   $user->attachRole($role);
                }
            }
            //dd($request->all());
            //return 0;


            \Session::flash('flash_message','Đã tạo người dùng '.$user->name .' thành công!');
            return redirect()->route('admin.user.index');
        } catch (\Illuminate\Database\QueryException $e) {
            if($e->errorInfo[1]==1062){
                return Redirect::back()->withErrors('Dữ liệu nhập trùng với dữ liệu trong cơ sở diệu');
            }
            return Redirect::back()->withErrors($e->errorInfo[2]);
        }

    }

    //Show role edit page
    //Method get
    public function edit($user){
        $user = User::findOrFail($user);
        $roles = Role::all();
        return view('admin.user.edit',compact('user','roles'));
    }

    //Update a role
    //Method put patch
    public function update($user, UserRequest $request){
        
        $user = User::findOrFail($user);
        $roles = Role::all();
        $user->roles()->sync([]); 
        foreach ($roles as $role) {
                //echo $request->input($role->id).PHP_EOL;
                if($request->input($role->id)){
                   $user->attachRole($role);
                }
            }
        if($request->has('password')){
            $user->password = bcrypt($request->input('password'));
        }
        //else return "Mật khẩu rỗng";
        $user->name = $request->input('name');
        $user->save();
        \Session::flash('flash_message','Đã sửa người dùng thành công!');       

        return Redirect::back();
    }
    
    //Remove a role
    //Method delete
    public function destroy($user){
        User::whereId($user)->delete();
        \Session::flash('flash_message','Đã xóa người dùng thành côngs!');
        return redirect()->route('admin.user.index');

    }
}
