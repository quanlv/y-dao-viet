<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use Redirect;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    //Return index, show list of roles
    //Method get
    public function index(){
        $permissions = Permission::all();
    	$roles = Role::paginate(20);    	
    	return view('admin.role.index',compact('roles','permissions'));
    }

    //Show role details
    //Method get
    public function show($role){
        $role = Role::findOrFail($role);
    	return view('admin.role.show',compact('role'));
    }

	//Return create role page
    //Method get
    public function create(){
    	$roles = Role::all();
        $permissions = Permission::all();
    	return view('admin.role.create',compact('roles','permissions'));
    }

    //Store a role to database
    //Method post
    public function store(RoleRequest $request){
        try {
            
            $role =  Role::create($request->all());

            $permissions = Permission::all();
            foreach ($permissions as $permission) {
                echo $request->input($permission->id).PHP_EOL;
                if($request->input($permission->id)){
                   $role->attachPermission($permission);
                }
            }
            //dd($request->all());
            //return 0;


            \Session::flash('flash_message','Đã tạo vai '.$role->name .' thành công!');
            return redirect()->route('admin.role.index');
        } catch (\Illuminate\Database\QueryException $e) {
            if($e->errorInfo[1]==1062){
                return Redirect::back()->withErrors('Dữ liệu nhập trùng với dữ liệu trong cơ sở diệu');
            }
            return Redirect::back()->withErrors($e->errorInfo[2]);
        }

    }

    //Show role edit page
    //Method get
    public function edit($role){
        $role = Role::findOrFail($role);
        $permissions = Permission::all();
    	return view('admin.role.edit',compact('role','permissions'));
    }

    //Update a role
    //Method put patch
    public function update($role, RoleRequest $request){
        $role = Role::findOrFail($role);
        $permissions = Permission::all();
        $role->perms()->sync([]); 
        foreach ($permissions as $permission) {
                //echo $request->input($permission->id).PHP_EOL;
                if($request->input($permission->id)){
                   $role->attachPermission($permission);
                }
            }
        $role->update($request->all());
        \Session::flash('flash_message','Đã sửa vai thành công!');
        

        return Redirect::back();
    }
    
    //Remove a role
    //Method delete
    public function destroy($role){
        Role::whereId($role)->delete();
        // $role->users()->sync([]); // Delete relationship data
        // $role->perms()->sync([]); // Delete relationship data
        // $role = Role::findOrFail($role);
        // Regular Delete
        //$role->delete(); // This will work no matter what


        // // Force Delete
        
        

        // $role->forceDelete(); // Now force delete will work regardless of whether the pivot table has cascading delete
        \Session::flash('flash_message','Đã xóa vai thành côngs!');
        return redirect()->route('admin.role.index');

    }

    

}
