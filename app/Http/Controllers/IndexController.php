<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Response;
use App\Register;

class IndexController extends Controller
{
	public function index(){
		$news = Category::where('slug', '=' ,'tin-tuc')->first()->post()->orderBy('id', 'DESC')->take(5)->get();
        //$posts = $news->post()->get();
		//$news = Post::orderBy('id', 'DESC')->take(5)->get();
        $manual = Category::where('slug', '=' ,'cam-nang')->first()->post()->orderBy('id', 'DESC')->take(5)->get();
    	return view('frontend.index', compact('news', 'manual'));
    }

    public function about(){
    	return view('frontend.page.about');
    }

    public function contact(){
    	return view('frontend.page.contact');
    }
    public function sale(){
    	//return view('frontend.page.sale');
		$news = Category::where('slug', '=' ,'khuyen-mai')->first();
		$posts = $news->post()->get();
		$title = "Cẩm nang";
		return view('frontend.page.sale', compact('posts', 'title'));
    }

     public function registerMedicalApplication(){
    	return view('frontend.patient.registerMedicalApplication');
    }
    public function registerMember(){
    	return view('frontend.patient.registerMember');
    }

    public function registerDoctor(){
    	return view('frontend.doctor.register');
    }

	public function sendRegisterDoctor(Request $request){
		\App\DoctorRegister::create($request->all());
		\Session::flash('message', 'Cảm ơn bạn đã đăng ký. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.');
        return redirect()->back();
	}

     public function doctorPolicy(){
        return view('frontend.doctor.policy');
    }

    public function handBook(){
		$news = Category::where('slug', '=' ,'cam-nang')->first();
        $posts = $news->post()->get();
		$title = "Cẩm nang";
    	return view('frontend.page.news', compact('posts', 'title'));
    }

	public function recruit(){
		$news = Category::where('slug', '=' ,'tuyen-dung')->first();
        $posts = $news->post()->get();
		$title = "Tuyển dụng";
    	return view('frontend.page.news', compact('posts', 'title'));
    }

    public function services(){
    	return view('frontend.page.services');
    }
    public function medicalInstruments(){
    	return view('frontend.page.medicalInstruments');
    }
    public function medical(){
    	return view('frontend.page.medical');
    }

    public function news(){
		$news = Category::where('slug', '=' ,'tin-tuc')->first();
        $posts = $news->post()->get();
		$title = "Tin tức";
    	return view('frontend.page.news', compact('posts', 'title'));
    }

	public function doctorList(){
		$doctors = \App\Doctor::all();
    	return view('frontend.doctor.list', compact('doctors'));
	}

    public function sitemap(){
        $domDocument = new \DOMDocument('1.0', "UTF-8");
        $domElement = $domDocument->createElement('urlset');
        $domAttribute = $domDocument->createAttribute('xmlns');
        $domAttribute->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
        $domElement->appendChild($domAttribute);
        $domDocument->appendChild($domElement);

        $pages = [
            'about',
            'sale',
            'dang-ky-kham',
            'dang-ky-thanh-vien',
            'thong-tin-benh-nhan',
            'dang-ky-bac-sy',
            'quy-dinh',
            'san-pham',
            'dung-cu-y-te',
            'thuoc',
            'tin-tuc',
            'cam-nang',
            'lien-he',
            ];

        foreach ($pages as $page){
            $pageUrl = url('http://ydaoviet.com').'/'.$page;
            $urlElement = $domDocument->createElement('url');
            $domElement->appendChild($urlElement);
            $locElement = $domDocument->createElement('loc',$pageUrl);
            $urlElement->appendChild($locElement);
            }

        $posts = Post::all();
        foreach ($posts as $post){
            $postUrl = $post->id();
            $urlElement = $domDocument->createElement('url');
            $domElement->appendChild($urlElement);
            $locElement = $domDocument->createElement('loc',$postUrl);
            $urlElement->appendChild($locElement);
            }

        return Response::make($domDocument->saveXML(), '200')->header('Content-Type', 'text/xml');
    }

    public function sendRegisterMedicalApplication(Request $request){
        Register::create($request->all());
		\Session::flash('message', 'Cảm ơn bạn đã đăng ký. Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất.');
        return redirect()->back();
    }

	public function viewPost($slug){
		$post = Post::where('slug', '=', $slug)->first();
		return view('frontend.post.index', compact('post'));
		//return $post;
	}
}
