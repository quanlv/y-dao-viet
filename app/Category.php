<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function post(){
    	return $this->belongsToMany('\App\Post','post_category','category_id','post_id');
    }

    protected $fillable = [
        'name', 'slug', 'description', 'parent_category_id'
    ];
}
