<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorRegister extends Model
{
    protected $guarded = ['id'];
}
