@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    
                    <td>User</td>
                    <td>Role</td>
                    <td>HasRole</td>
                </tr>
            </thead>
            <tbody>

                @foreach($users as $user)
                    @foreach($roles as $role)
                    <tr>
                        
                        <td>{{ $user->email }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $user->hasRole($role->name) }}</td>
                    </tr>
                    @endforeach
                @endforeach



            </tbody>
        </table>
        <table class="table">
            <thead>
                <tr>
                    
                    <td>Role</td>
                    <td>Permission</td>
                    <td>hasPermission</td>
                </tr>
            </thead>
            <tbody>

                @foreach($roles as $role)
                    @foreach($permissions as $permission)
                    <tr>
                        
                        <td>{{ $role->name }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $role->hasPermission($permission->name) }}</td>
                        
                    </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>

        <table class="table">
            <thead>
                <tr>
                    
                    <td>User</td>
                    <td>Permission</td>
                    <td>Can</td>
                </tr>
            </thead>
            <tbody>


                @foreach($users as $user)
                    @foreach($permissions as $permission)
                    <tr>
                        
                        <td>{{ $user->email }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $user->can($permission->name) }}</td>
                        
                    </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection
