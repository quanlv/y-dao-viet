<!DOCTYPE html>
<html lang="vi">
	<head>

		@include('frontend.inc.head')
		@yield('css')
		@yield('js')

	</head>
	<body>
	<style>

#floating-phone { display: block; position: fixed; left: 10px; bottom: 10px; height: 50px; width: 50px; background: #46C11E url(http://callnowbutton.com/phone/callbutton01.png) center / 30px no-repeat; z-index: 99; color: #FFF; font-size: 35px; line-height: 55px; text- align: center; border-radius: 50%; -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.5); -moz-box-shadow: 0 2px 5px rgba(0,0,0,.5); box-shadow: 0 2px 5px rgba(0,0,0,.5); }

@media (max-width: 650px) { #floating-phone { display: block; } }

</style>



<a href="tel:0936326158" title="Gọi 0936.326.158" onclick="_gaq.push(['_trackEvent', 'Contact', 'Call Now Button', 'Phone']);" id="floating-phone">

<i class="uk-icon-phone"></i></a>

		<div class="body">
			<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': false, 'stickyEnableOnBoxed': false, 'stickyEnableOnMobile': false, 'stickyStartAt': 35, 'stickySetTop': '-35px', 'stickyChangeLogo': false}">
				@include('frontend.inc.header')
			</header>

			<div role="main" class="main">
				@yield('main')
			</div>
			<footer id="footer" class="m-none">
				@include('frontend.inc.footer')
			</footer>
		</div>

		@include('frontend.inc.script')



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Đăng nhập</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						<label for="phone" class="col-md-4 control-label">Số điện thoại</label>

						<div class="col-md-6">
							<input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

							@if ($errors->has('phone'))
							<span class="help-block">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Mật khẩu</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="password" required>

							@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lưu đăng nhập
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Đăng nhập
							</button>

							<a class="btn btn-link" href="{{ route('password.request') }}">
								Quên mật khẩu?
							</a>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
			</div>
		</div>

	</div>
</div>
<!-- ENd modal -->

<div id="registerMedicalApplication" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Đăng ký khám</h4>
			</div>
			<div class="modal-body">
				<form action="/dang-ky-kham" id="frmSignUp" method="post">
					{{ csrf_field() }}
						<div class="form-group">

								<label>Họ tên</label>
								<input type="text" name="name" class="form-control input-lg">

						</div>
						<div class="form-group">

								<label>Số điện thoại</label>
								<input type="text" name="telephone" class="form-control input-lg">

						</div>
						<div class="form-group">
							<label for="sel1">Gói dịch vụ</label>
							<select class="form-control input-lg" name="pack"id="sel1">
								<option disabled="" selected="" />
								<option value="10"> Gói 10 buổi</option>
								<option value="5"> Gói 5 buổi</option>
								<option value="3"> Gói 3 buổi</option>
								<option value="0"> Dùng thử</option>
							</select>

						</div>


						<div class="form-group">

							<button type="submit" class="btn btn-success btn-block btn-lg" >
								Đăng ký
							</button>

						</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
			</div>
		</div>

	</div>
</div>

<!-- End modal 2 -->
	<script type='text/javascript'>window._sbzq||function(e){e._sbzq=[];var t=e._sbzq;t.push(["_setAccount",20479]);var n=e.location.protocol=="https:"?"https:":"http:";var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//static.subiz.com/public/js/loader.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)}(window);</script>
	</body>
</html>
