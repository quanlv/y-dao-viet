@extends('frontend.layouts.master')
@section('title')
Liên hệ
@endsection
@section('main')
<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Liên hệ<span>Liên hệ với chúng tôi ngay hôm nay để nhận nhiều ưu đãi</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Liên hệ</li>
							</ul>
						</div>
					</div>
				</div>
			</section>

			<div class="container">

				<div class="row mt-lg">
					<div class="col-md-4">
						<div class="feature-box feature-box-style-2">
							<div class="feature-box-icon">
								<i class="icon-location-pin icons"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="mb-sm">Địa chỉ</h4>
								<p class="font-size-lg">
									C10, Ngõ 193 Trung Kính, Yên Hoà, Cầu Giấy, Hà Nội<br>
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="feature-box feature-box-style-2">
							<div class="feature-box-icon">
								<i class="icon-phone icons"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="mb-sm">Hotline </h4>
								<p class="font-size-lg">
									0936326158 <br>
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="feature-box feature-box-style-2">
							<div class="feature-box-icon">
								<i class="icon-envelope icons"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="mb-sm">Email</h4>
								<p class="font-size-lg">
									<a href="mailto:ydaoviet@gmail.com" class="text-decoration-none">ydaoviet@gmail.com</a>
								</p>
							</div>
						</div>
					</div>
				</div>

				<hr class="solid">

				<div class="row pt-lg mb-lg pb-xl">
					<div class="col-md-6">
						<div id="googlemaps" class="google-map m-none mb-xl"></div>
					</div>
					<div class="col-md-6">

						<h4 class="font-weight-semibold mb-xlg">Thông tin phản hồi</h4>

						<div class="alert alert-success hidden mt-lg" id="contactSuccess">
							<strong>Success!</strong> Your message has been sent to us.
						</div>

						<div class="alert alert-danger hidden mt-lg" id="contactError">
							<strong>Error!</strong> There was an error sending your message.
							<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
						</div>

						<form id="contactForm" action="php/contact-form.php" method="POST">
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<input type="text" placeholder="Họ tên" value="" data-msg-required="Vui lòng nhập tên của bạn" maxlength="100" class="form-control" name="name" id="name" required>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<input type="email" placeholder="E-mail" value="" data-msg-required="Vui lòng nhập địa chỉ Email." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
									</div>
								</div>
							</div> -->
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<input type="text" placeholder="Chủ đề" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-12">
										<textarea maxlength="5000" placeholder="Tin nhắn" data-msg-required="Nhập tin nhắn" rows="5" class="form-control" name="message" id="message" required></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Gửi tin nhắn" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
								</div>
							</div>
						</form>

					</div>
				</div>

			</div>

@endsection()
