@extends('frontend.layouts.master')
@section('title')
Giới thiệu
@endsection
@section('main')
<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Công ty cổ phần Y Đạo Việt <span>Tiên phong trong lĩnh vực bác sỹ gia đình</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Về chúng tôi</li>
							</ul>
						</div>
					</div>
				</div>
			</section>

			<div class="container">

				<div class="row mt-xl">
					<div class="col-md-8">
						<h2 class="font-weight-semibold mb-xs">Giới thiệu </h2>

						<p>Công ty Cổ phần Y Đạo Việt thành lập vào ngày 10 tháng 8 năm 2017.</p>
						<p>Công ty Y Đạo Việt là công ty đầu tiên trong ngành y tế kết nối giữa những nhân viên hành nghề y học cổ truyền và khách hàng.Công ty cổ phần Y Đạo Việt phát triển và xây dựng phần mềm quản trị, kết nối người cung cấp dịch vụ bấm huyệt, châm cứu tại nhà (Y, bác sỹ, lương y hành nghề Y học cổ truyền) và người bệnh (mắc các bệnh về thoái hoá khớp...).
						</p>
						<p>Tên công ty viết bằng tiếng Việt : <a style="color: blue">CÔNG TY CỔ PHẦN Y ĐẠO VIỆT</a></p>
						<p>Tên công ty viết bằng tiếng anh: <a style="color: blue">Y DAO VIET JOINT STOCK COMPANY</a></p>
						<p>Trụ sở chính: C10, Ngõ 193 Trung Kính, Yên Hoà, Cầu Giấy, Hà Nội </p>
						<p>Website: <a href="/">http://ydaoviet.com</a></p>
						<p>Điện thoại: 0936.326.158</p>
						<p>Địa chỉ phòng khám: C10, Ngõ 193 Trung Kính, Yên Hoà, Cầu Giấy, Hà Nội.</p>
					</div>
					<div class="col-md-4">
						<div class="owl-carousel owl-theme nav-inside" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
							<div>
								<img src="images/sp/bam_huyet.jpg" alt="" class="img-responsive box-shadow-custom" />
							</div>
							<div>
								<img src="images/sp/cham_cuu.jpg" alt="" class="img-responsive box-shadow-custom" />
							</div>
							<div>
								<img src="images/sp/xong_da_muoi.jpg" alt="" class="img-responsive box-shadow-custom" />
							</div>
						</div>

					</div>
				</div>

				<div class="row mb-xlg">
					<div class="col-md-12">
						<div class="toggle toggle-primary toggle-simple" data-plugin-toggle>
							<section class="toggle">
								<label>Tầm nhìn</label>
								<div class="toggle-content">
									<p>Trở thành công ty tiên phong trong cung cấp dịch vụ chăm sóc sức khoẻ tại nhà cho người dân Việt Nam, đặc biệt là các dịch vụ chăm sóc sức khoẻ bằng phương pháp bấm huyệt, châm cứu, hoả trị liệu..., góp phần cải thiện chất lượng cuộc sống, chất lượng lao động, tạo thêm giá trị cho cộng đồng nói chung, cho mỗi người dân nói riêng.
									Ngoài ra còn trở thành tổ chức điều hành/quản trị các diễn đàn của NVYT và khách hàng trong chăm sóc sức khoẻ nhằm lan toả những giá trị của dịch vụ, những thông tin bổ ích trong chăm sóc sức khoẻ nói chung, chăm sóc cơ - xương - khớp cho người bệnh nói riêng.
									</p>
								</div>
							</section>
							<section class="toggle">
								<label>Sứ mệnh</label>
								<div class="toggle-content">
									<p>Công ty không ngừng cải thiện chất lượng dịch vụ chăm sóc sức khoẻ tại nhà cho người dân bằng phương pháp bám huyệt, châm cứu, hoả trị liệu... Công ty cũng sẽ cải tiến không ngừng, áp dụng công nghệ thông tin trong việc mở rộng qui mô chăm sóc sức khoẻ tại nhà.
									Y đạo việt là cầu nối giữa các bác sỹ với người bệnh, xây dựng mạng lưới các bác sỹ châm cứu bấm huyệt tại nhà trên toàn quốc.
									</p>
								</div>
							</section>
							<section class="toggle">
								<label>Mục tiêu</label>
								<div class="toggle-content">
									<p>Trong vòng 2 năm tới, sẽ bao phủ thị trường Hà Nội trong cung cấp dịch vụ bấm huyệt, châm cứu, hoả trị liệu tại nhà với dịch vụ được chuẩn hoá.</p>
									<p>Trong vòng 2 năm tiếp theo sẽ bao phủ thị trường TP Hồ Chí Minh, Đà Nẵng cung cấp dịch vụ bấm huyệt, châm cứu, hoả trị liệu tại nhà.</p>
									<p>Trong vòng 3 năm tiếp theo sẽ bao phủ toàn bộ 64 tỉnh/thành với các dịch vụ được chuẩn mực hoá.</p>
								</div>
							</section>
						</div>
					</div>
				</div>

			</div>

			@include('frontend.inc.contactus')
@endsection()
