@extends('frontend.layouts.master')
@section('title')
{{ $title }}
@endsection
@section('main')
			<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">{{ $title }} <span></span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">{{ $title }}</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<div class="container">

					<div class="row">
						<div class="col-md-9">

							<div class="row">
								<div class="col-md-12">

									<h4 class="mb-none">Bài viết</h4>
									<hr class="solid mt-md">
								</div>
							</div>

							@foreach ($posts as $post)
							<div class="row mt-xl">
							<div class="col-xs-4">
								<a href="/post/{{ $post->slug }}" class="text-decoration-none">
									<img alt="" class="img-responsive" src="{{ $post->avatar }}">
								</a>
							</div>
							<div class="col-sm-8">
								<h4 class="font-weight-semibold mb-xs mt-xs">
									<a href="/post/{{ $post->slug }}" class="text-decoration-none">{{ $post->title }}</a>
								</h4>
								<p>{{ mb_substr(strip_tags($post->content), 0, 200, 'utf8'). '...' }}</p>
								<p><a href="/post/{{ $post->slug }}" class="btn btn-borders btn-quaternary custom-button text-uppercase mt-sm mb-md font-weight-bold btn-sm">xem thêm...</a></p>
							</div>
						</div>
						@endforeach







						</div>
						<div class="col-md-3">
							@include('frontend.inc.sidebar')
						</div>


				</div>

@endsection()
