@extends('frontend.layouts.master')
@section('title')
Sản phẩm
@endsection
@section('main')
			<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Sản phẩm<span>Các gói dịch vụ chính của chúng tôi</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Sản phẩm</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="team">
				<div class="container">

					<div class="row mt-lg mb-lg">
						<div class="col-md-12">


							<ul class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
								<li data-option-value="*" class="active"><a href="#">Tất cả</a></li>
								<li data-option-value=".cardiology"><a href="#">Dịch vụ</a></li>
								<li data-option-value=".gastroenterology"><a href="#">Thuốc</a></li>
								<li data-option-value=".pulmonology"><a href="#">Thiết bị y tế</a></li>
							</ul>

							<hr class="solid">

							<div class="row">

								<div class="sort-destination-loader sort-destination-loader-showing">
									<ul class="portfolio-list sort-destination" data-sort-id="portfolio">
										<li class="col-md-3 col-sm-6 col-xs-12 isotope-item cardiology">
											<div class="portfolio-item">
												<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
													<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
														<span class="thumb-info-wrapper m-none">
															<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
														</span>
														<span class="thumb-info-caption p-md pt-xlg pb-xlg">
															<span class="custom-thumb-info-title">
																<span class="custom-thumb-info-type font-weight-light text-md">Dịch vụ</span>
																<span class="custom-thumb-info-inner font-weight-semibold text-lg">Châm cứu</span>
																<i class=" icons font-weight-semibold text-lg ">100$</i>
															</span>
														</span>
													</span>
												</a>
											</div>
										</li>
										<li class="col-md-3 col-sm-6 col-xs-12 isotope-item gastroenterology">
											<div class="portfolio-item">
												<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
													<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
														<span class="thumb-info-wrapper m-none">
															<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
														</span>
														<span class="thumb-info-caption p-md pt-xlg pb-xlg">
															<span class="custom-thumb-info-title">
																<span class="custom-thumb-info-type font-weight-light text-md">Thuốc</span>
																<span class="custom-thumb-info-inner font-weight-semibold text-lg">Bình vị nam</span>
																<i class=" icons font-weight-semibold text-lg ">100$</i>
															</span>
														</span>
													</span>
												</a>
											</div>
										</li>
										<li class="col-md-3 col-sm-6 col-xs-12 isotope-item pulmonology">
											<div class="portfolio-item">
												<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
													<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
														<span class="thumb-info-wrapper m-none">
															<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
														</span>
														<span class="thumb-info-caption p-md pt-xlg pb-xlg">
															<span class="custom-thumb-info-title">
																<span class="custom-thumb-info-type font-weight-light text-md">Thiết bị y tế</span>
																<span class="custom-thumb-info-inner font-weight-semibold text-lg">Kẹp y tế</span>
																<i class=" icons font-weight-semibold text-lg ">100$</i>
															</span>
														</span>
													</span>
												</a>
											</div>
										</li>
									</ul>
								</div>

							</div>

						</div>
					</div>

				</div>
			</section>

@include('frontend.inc.contactus')
@endsection()