@extends('frontend.layouts.master')
@section('title')
Khuyến mại
@endsection
@section('main')
<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Khuyến mại <span>Tin tức khuyến mại trong tuần</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Khuyến mại</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
			<div class="row">
				<div class="col-md-8">
					@foreach ($posts as $post)
					<div class="row mt-xl">
					<div class="col-xs-4">
						<a href="/post/{{ $post->slug }}" class="text-decoration-none">
							<img alt="" class="img-responsive" src="{{ $post->avatar }}">
						</a>
					</div>
					<div class="col-sm-8">
						<h4 class="font-weight-semibold mb-xs mt-xs">
							<a href="/post/{{ $post->slug }}" class="text-decoration-none">{{ $post->title }}</a>
						</h4>
						<p>{{ mb_substr(strip_tags($post->content), 0, 200, 'utf8'). '...' }}</p>
						<p><a href="/post/{{ $post->slug }}" class="btn btn-borders btn-quaternary custom-button text-uppercase mt-sm mb-md font-weight-bold btn-sm">xem thêm...</a></p>
					</div>
				</div>
				@endforeach


				</div>
				<div class="col-md-3 col-md-offset-1">
					<ul class="portfolio-list sort-destination" data-sort-id="portfolio">
							<li class="row isotope-item cardiology">
								<div class="portfolio-item">
									<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
										<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
											<span class="thumb-info-wrapper m-none">
												<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
											</span>
											<span class="thumb-info-caption p-md pt-xlg pb-xlg">
													<span class="custom-thumb-info-title">
													<span class="custom-thumb-info-inner font-weight-semibold text-lg">Gói dịch vụ 10 ngày</span><br>
														<span style="color: black;  text-decoration: line-through;padding-left: 15px;" class="custom-thumb-info-type font-weight-light text-md"> Giá cũ : 250.000</span><br>
														<span style="color: red;padding-left: 15px" class="custom-thumb-info-type font-weight-light text-md"> Giá mới : 100.000</span><br>
													</span>
										</span>
									</a>
								</div>
							</li>
							<li class="row isotope-item cardiology">
								<div class="portfolio-item">
									<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
										<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
											<span class="thumb-info-wrapper m-none">
												<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
											</span>
											<span class="thumb-info-caption p-md pt-xlg pb-xlg">
													<span class="custom-thumb-info-title">
													<span class="custom-thumb-info-inner font-weight-semibold text-lg">Thuốc Bình Vị Nam</span><br>
														<span style="color: black;  text-decoration: line-through;padding-left: 15px;" class="custom-thumb-info-type font-weight-light text-md"> Giá cũ : 250.000</span><br>
														<span style="color: red;padding-left: 15px" class="custom-thumb-info-type font-weight-light text-md"> Giá mới : 100.000</span><br>
													</span>
										</span>
									</a>
								</div>
							</li>

							<li class="row isotope-item cardiology">
								<div class="portfolio-item">
									<a href="#" data-href="demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
										<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
											<span class="thumb-info-wrapper m-none">
												<img src="/frontend/img/demos/medical/doctors/doctor-1.jpg" class="img-responsive" alt="">
											</span>
											<span class="thumb-info-caption p-md pt-xlg pb-xlg">
													<span class="custom-thumb-info-title">
													<span class="custom-thumb-info-inner font-weight-semibold text-lg">Gói dịch vụ 5 ngày</span><br>
														<span style="color: black;  text-decoration: line-through;padding-left: 15px;" class="custom-thumb-info-type font-weight-light text-md"> Giá cũ : 250.000</span><br>
														<span style="color: red;padding-left: 15px" class="custom-thumb-info-type font-weight-light text-md"> Giá mới : 100.000</span><br>
													</span>
										</span>
									</a>
								</div>
							</li>

					</ul>
				</div>

			</div>
			</div>

@include('frontend.inc.contactus')
@endsection()
