@extends('frontend.layouts.master')
@section('title')
Kiến thức y khoa
@endsection
@section('main')
			<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Kiến thức y khoa <span>Thông tin sức khỏe hữu ích dành cho mọi người</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Kiến thức y khoa</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<div class="container">

					<div class="row">
						<div class="col-md-9">

							<div class="row">
								<div class="col-md-12">

									<h4 class="mb-none">Posts</h4>
									<hr class="solid mt-md">
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="recent-posts">
										<article class="post">
											<div class="date">
												<span class="day">15</span>
												<span class="month">Jan</span>
											</div>
											<h4><a href="blog-post.html">Kiến thức y khoa 1</a></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
										</article>
									</div>
								</div>
								<div class="col-md-4">
									<div class="recent-posts">
										<article class="post">
											<div class="date">
												<span class="day">15</span>
												<span class="month">Jan</span>
											</div>
											<h4><a href="blog-post.html">Kiến thức y khoa 1</a></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
											<div class="post-meta">
												<span><i class="fa fa-calendar"></i> January 10, 2017 </span>
												<span><i class="fa fa-user"></i> By <a href="#">John Doe</a> </span>
												<span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a> </span>
												<span><i class="fa fa-comments"></i> <a href="#">12 Comments</a></span>
												<hr class="solid">
												<a href="blog-post.html" class="btn btn-xs btn-primary pull-right mb-lg">Read more...</a>
											</div>
										</article>
									</div>
								</div>
								<div class="col-md-4">
									<div class="recent-posts">
										<article class="post">
											<div class="owl-carousel owl-theme nav-inside pull-left mr-lg mb-sm" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
												<div>
													<img alt="" class="img-responsive img-rounded" src="img/blog/blog-image-2.jpg">
												</div>
												<div>
													<img alt="" class="img-responsive img-rounded" src="img/blog/blog-image-1.jpg">
												</div>
											</div>
											<div class="date">
												<span class="day">15</span>
												<span class="month">Jan</span>
											</div>
											<h4><a href="blog-post.html">Kiến thức y khoa 1</a></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
										</article>
									</div>
								</div>
							</div>

							<div class="row mt-xlg">
								<div class="col-md-12">
									<h4>Recent Posts</h4>
									<div class="owl-carousel owl-theme show-nav-title top-border" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 3}}, 'items': 3, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Kiến thức y khoa 1</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Tin tức cũ thứ 2.</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Kiến thức y khoa 1</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Tin tức cũ thứ 4.</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Tin tức cũ thứ 5.</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Tin tức cũ thứ 6.</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div>
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4><a href="blog-post.html">Tin tức cũ thứ 7.</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. Lorem ipsum amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-3">
							@include('frontend.inc.sidebar')
						</div>
					</div>

				</div>

@include('frontend.inc.contactus')
@endsection()