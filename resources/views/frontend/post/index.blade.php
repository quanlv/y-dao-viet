
@extends('frontend.layouts.master')
@section('title')
{{ $post->title }}
@endsection
@section('main')
<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">{{ $post->title }}</h1>
						</div>
					</div>
				</div>
			</section>

			<section class="team">
				<div class="container">

					<div class="row mt-lg mb-lg">
						<div class="col-md-12">

							<span class="font-weight-semibold mb-xs">{{ $post->title }}</span>

							<div class="content">
                                {!! $post->content !!}

							</div>




						</div>
					</div>

				</div>
			</section>

			<div class="container">

			</div>
@endsection()
