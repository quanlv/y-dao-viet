@extends('frontend.layouts.master')
@section('title')
Y Đạo Việt – Kết Nối Y Đạo Lan Tỏa Cuộc Sống
@endsection
@section('main')
	<div class="slider-container rev_slider_wrapper" style="height: 650px;">
				<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 650, 'disableProgressBar': 'on', 'navigation': {'bullets': {'enable': true, 'direction': 'vertical', 'h_align': 'right', 'v_align': 'center', 'space': 5}, 'arrows': {'enable': false}}}">
					<ul>
						<li data-transition="fade">
							<img src="/images/slide1.jpg"
								alt=""
								data-bgposition="center center"
								data-bgfit="cover"
								data-bgrepeat="no-repeat"
								class="rev-slidebg">

							<div class="tp-caption main-label"
								data-x="left" data-hoffset="25"
								data-y="center" data-voffset="-5"
								data-start="1500"
								data-whitespace="nowrap"
								data-transform_in="y:[100%];s:500;"
								data-transform_out="opacity:0;s:500;"
								style="z-index: 5; font-size: 1.5em; text-transform: uppercase;"
								data-mask_in="x:0px;y:0px;">Tiên phong trong lĩnh vực bác sỹ gia đình</div>

							<div class="tp-caption main-label"
								data-x="left" data-hoffset="25"
								data-y="center" data-voffset="-55"
								data-start="500"
								style="z-index: 5; text-transform: uppercase; font-size: 52px;"
								data-transform_in="y:[-300%];opacity:0;s:500;">Công ty Y Đạo Việt</div>

							<div class="tp-caption bottom-label"
								data-x="left" data-hoffset="25"
								data-y="center" data-voffset="40"
								data-start="2000"
								style="z-index: 5; border-bottom: 1px solid #fff; padding-bottom: 3px;"
								data-transform_in="y:[100%];opacity:0;s:500;" style="font-size: 1.2em;">Chúng tôi luôn bên bạn</div>
						</li>
						<li data-transition="fade">
							<img src="/images/slide2.jpg"
								alt=""
								data-bgposition="center center"
								data-bgfit="cover"
								data-bgrepeat="no-repeat"
								class="rev-slidebg">

							<div class="tp-caption main-label"
								data-x="left" data-hoffset="25"
								data-y="center" data-voffset="-5"
								data-start="1500"
								data-whitespace="nowrap"
								data-transform_in="y:[100%];s:500;"
								data-transform_out="opacity:0;s:500;"
								style="z-index: 5; font-size: 1.5em; text-transform: uppercase;"
								data-mask_in="x:0px;y:0px;">Hãy đến với chúng tôi</div>

							<div class="tp-caption main-label"
								data-x="left" data-hoffset="25"
								data-y="center" data-voffset="-55"
								data-start="500"
								style="z-index: 5; text-transform: uppercase; font-size: 52px;"
								data-transform_in="y:[-300%];opacity:0;s:500;">Công ty Y Đạo Việt</div>


						</li>
					</ul>
				</div>
			</div>
			<section class="section-custom-medical">
				<div class="container">
					<div class="row mt-xlg pt-xlg mb-xlg pb-xs">
						<div class="col-sm-8 col-md-8">
							<h2 class="font-weight-semibold mb-xs">Về chúng tôi</h2>
							<p class="lead font-weight-normal">Công ty Y Đạo Việt</p>

							<p>Công ty Y Đạo Việt là công ty đầu tiên trong ngành y tế kết nối giữa những nhân viên hành nghề y học cổ truyền và khách hàng. Công ty cổ phần Y Đạo Việt phát triển và xây dựng phần mềm quản trị, kết nối người cung cấp dịch vụ bấm huyệt, châm cứu tại nhà (Y, bác sỹ, lương y hành nghề Y học cổ truyền) và người bệnh (mắc các bệnh về thoái hoá khớp...) </p>

							<a class="btn btn-borders btn-quaternary custom-button text-uppercase mt-lg mb-lg font-weight-bold" href="/gioi-thieu">Chi tiết</a>
						</div>
						<div class="col-sm-4 col-md-4">
							<img src="/images/y-dao-viet-gioi-thieu.jpg" alt class="img-responsive box-shadow-custom" />

						</div>
					</div>
				</div>
			</section>
			<section class="section">
				<div class="container">
					<div class="row mt-md">
						<div class="col-md-12">
							<h2 class="font-weight-semibold m-none">Các gói dịch vụ</h2>
							<p class="lead font-weight-normal">Các gói dịch vụ cho bệnh nhân</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<a href="#" class="text-decoration-none">
								<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom thumb-info-no-zoom thumb-info-side-image-custom-highlight">
									<span class="thumb-info-side-image-wrapper">
										<img alt="" class="img-responsive" src="/images/sp/bam_huyet.jpg">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text p-xl">
											<h4 class="font-weight-semibold mb-xs">Bấm huyệt</h4>
											<p>Bấm huỵệt làm tan cơn đau nhức bởi thoái hoá cột sống, thoát vị đĩa đệm.</p>
										</span>
									</span>
								</span>
							</a>
						</div>
						<div class="col-sm-4 col-md-4">
							<a href="#" class="text-decoration-none">
								<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom thumb-info-no-zoom thumb-info-side-image-custom-highlight">
									<span class="thumb-info-side-image-wrapper">
										<img alt="" class="img-responsive" src="/images/sp/cham_cuu.jpg">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text p-xl">
											<h4 class="font-weight-semibold mb-xs">Châm cứu</h4>
											<p>Châm cứu là biện pháp tác động đến các huyệt đạo, khai thông kinh mạch, giảm nhanh các cơn đau</p>
										</span>
									</span>
								</span>
							</a>
						</div>
						<div class="col-sm-4 col-md-4">
							<a href="#" class="text-decoration-none">
								<span class="thumb-info thumb-info-side-image thumb-info-side-image-custom thumb-info-no-zoom thumb-info-no-zoom thumb-info-side-image-custom-highlight">
									<span class="thumb-info-side-image-wrapper">
									<img alt="" class="img-responsive" src="/images/sp/xong_da_muoi.jpg">
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text p-xl">
											<h4 class="font-weight-semibold mb-xs">Xông hơi đá muối Hymalaya</h4>
											<p>Xông đá muối Hymalaya là một phương pháp làm tăng nhiệt cơ thể, giúp giải độc, khai thông khí huyết, kết hợp cùng với bấm huyệt sẽ giảm các cơn đau nhanh chóng</p>
										</span>
									</span>
								</span>
							</a>
						</div>
					</div>
					<!-- <div class="row pb-xlg">
						<div class="col-md-12 center">
							<a class="btn btn-borders btn-quaternary custom-button text-uppercase font-weight-bold" href="/dichvu">Chi tiết</a>
						</div>
					</div> -->
				</div>
			</section>

			<section class="team">
				<div class="container">
					<div class="row mt-xlg">
						<div class="col-md-12">
							<h2 class="font-weight-semibold m-none">Danh sách bác sỹ</h2>
							<p class="lead font-weight-normal">Các bác sỹ đến từ các bệnh viện hàng đầu Việt Nam về xoa bóp, bấm huyệt, khôi phục chức năng</p>

							<div id="porfolioAjaxBoxMedical" class="ajax-box ajax-box-init mb-lg">

								<div class="bounce-loader">
									<div class="bounce1"></div>
									<div class="bounce2"></div>
									<div class="bounce3"></div>
								</div>

								<div class="ajax-box-content" id="porfolioAjaxBoxContentMedical"></div>

							</div>

						</div>
					</div>
					<div class="row mb-xlg">
						<div class="owl-carousel owl-theme nav-bottom rounded-nav pl-xs pr-xs" data-plugin-options="{'items': 4, 'loop': false, 'dots': false, 'nav': true}">
							<div class="pr-sm pl-sm">
								<a href="/#" data-href="/demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
									<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper m-none">
											<img src="/images/bs2.png" class="img-responsive" alt="">
										</span>
										<span class="thumb-info-caption p-md pt-xlg pb-xlg">
											<span class="custom-thumb-info-title">
												<span class="custom-thumb-info-type font-weight-light text-md">Bệnh viện Bạch Mai</span>
												<span class="custom-thumb-info-inner font-weight-semibold text-lg">Phạm Trọng Huấn</span>
												<i class="icon-arrow-right-circle icons font-weight-semibold text-lg "></i>
											</span>
										</span>
									</span>
								</a>
							</div>
							<div class="pr-sm pl-sm">
								<a href="/#" data-href="/demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
									<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper m-none">
											<img src="/images/bs3.png" class="img-responsive" alt="">
										</span>
										<span class="thumb-info-caption p-md pt-xlg pb-xlg">
											<span class="custom-thumb-info-title">
												<span class="custom-thumb-info-type font-weight-light text-md">Bệnh viện Thanh Nhàn</span>
												<span class="custom-thumb-info-inner font-weight-semibold text-lg"> Nguyễn Thanh Nga</span>
												<i class="icon-arrow-right-circle icons font-weight-semibold text-lg "></i>
											</span>
										</span>
									</span>
								</a>
							</div>
							<div class="pr-sm pl-sm">
								<a href="/#" data-href="/demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
									<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper m-none">
											<img src="/images/bs4.png" class="img-responsive" alt="">
										</span>
										<span class="thumb-info-caption p-md pt-xlg pb-xlg">
											<span class="custom-thumb-info-title">
												<span class="custom-thumb-info-type font-weight-light text-md">Bệnh viện Việt Đức</span>
												<span class="custom-thumb-info-inner font-weight-semibold text-lg">Lê Văn Quân</span>
												<i class="icon-arrow-right-circle icons font-weight-semibold text-lg "></i>
											</span>
										</span>
									</span>
								</a>
							</div>
							<div class="pr-sm pl-sm">
								<a href="/#" data-href="/demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
									<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper m-none">
											<img src="/images/bs5.png" class="img-responsive" alt="">
										</span>
										<span class="thumb-info-caption p-md pt-xlg pb-xlg">
											<span class="custom-thumb-info-title">
												<span class="custom-thumb-info-type font-weight-light text-md">Viện Y học cổ truyền</span>
												<span class="custom-thumb-info-inner font-weight-semibold text-lg">Phạm Văn Tiệp</span>
												<i class="icon-arrow-right-circle icons font-weight-semibold text-lg "></i>
											</span>
										</span>
									</span>
								</a>
							</div>
							<div class="pr-sm pl-sm">
								<a href="/#" data-href="/demo-medical-doctors-detail.html" data-ajax-on-page class="text-decoration-none">
									<span class="thumb-info thumb-info-no-zoom thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper m-none">
											<img src="/images/bs2.png" class="img-responsive" alt="">
										</span>
										<span class="thumb-info-caption p-md pt-xlg pb-xlg">
											<span class="custom-thumb-info-title">
												<span class="custom-thumb-info-type font-weight-light text-md">Bệnh viện Bạch Mai</span>
												<span class="custom-thumb-info-inner font-weight-semibold text-lg">Trần Văn Tân</span>
												<i class="icon-arrow-right-circle icons font-weight-semibold text-lg "></i>
											</span>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="section section-no-border">
				<div class="container">
						<div class="col-md-6">

						<h2  href = "/tintuc" class="font-weight-semibold mb-xs">Tin tức</h2>
						@foreach ($news as $post)
							<div class="row mt-lg">
								<div class="feature-box feature-box-style-2 mb-xl appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
									<div class="feature-box-icon">
										<img src="{{ $post->avatar }}" alt class="img-responsive" />
									</div>
									<div class="feature-box-info ml-md">
										<h4 class="font-weight-semibold"><a href="{{ $post->getUrl() }}" class="text-decoration-none">{{ $post->title }}</a></h4>
										<p>{{ mb_substr(strip_tags($post->content), 0, 200, 'utf8'). '...' }}</p>
									</div>
								</div>
							</div>
						@endforeach

					</div>
					<div class="col-md-6">
						<h2 class="font-weight-semibold mb-xs">Cẩm nang sức khỏe</h2>
						@foreach ($manual as $post)
						<div class="row mt-lg">
							<div class="feature-box feature-box-style-2 mb-xl appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
								<div class="feature-box-icon">
									<img src="{{ $post->avatar }}" alt class="img-responsive" />
								</div>
								<div class="feature-box-info ml-md">
									<h4 class="font-weight-semibold"><a href="{{ $post->getUrl() }}" class="text-decoration-none">{{ $post->title }}</a></h4>
									<p>{{ mb_substr(strip_tags($post->content), 0, 200, 'utf8'). '...' }}</p>
								</div>
							</div>
						</div>
						@endforeach

					</div>
				</div>

			</section>
			<section>
				<div class="container">
					<div class="row mt-md">
						<div class="col-md-12">
							<h2 class="font-weight-semibold m-none">Sản phẩm bổ sung</h2>
							<p class="lead font-weight-normal">Sản phẩm bổ sung trọng liệu trình chữa bệnh</p>
						</div>
					</div>
					<div class="row mb-xlg pb-xlg">
						<div class="content-grid pl-md pr-md">
							<div class="content-grid-row">
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-1.png" alt class="img-responsive" />
								</div>
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-2.png" alt class="img-responsive" />
								</div>
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-3.png" alt class="img-responsive" />
								</div>
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-4.png" alt class="img-responsive" />
								</div>
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-5.png" alt class="img-responsive" />
								</div>
								<div class="content-grid-item col-sm-4 col-md-2 center">
									<img src="frontend/img/demos/medical/logos/insurance-logo-6.png" alt class="img-responsive" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
				@include('frontend.inc.contactus')

			<section>
				<div class="container">
					<div class="row mt-xlg pt-md mb-xlg pb-md">
						<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false, 'dots': false}">
							<div>
								<div class="col-md-8 col-md-offset-2 pt-xlg">
									<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
										<div class="testimonial-quote">“</div>
										<blockquote>
											<p class="font-weight-light">Y học cổ truyền (châm cứu, bấm huyệt kết hợp dùng thuốc) là phương pháp bảo tồn cột sống duy nhất</p>
										</blockquote>
										<div class="testimonial-author mt-xlg">
											<p class="text-uppercase">
												<strong>Nguyễn Thị Thanh Nga</strong>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="col-md-8 col-md-offset-2 pt-xlg">
									<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
										<div class="testimonial-quote">“</div>
										<blockquote>
											<p class="font-weight-light">Kết nối - Lan tỏa</p>
										</blockquote>
										<div class="testimonial-author mt-xlg">
											<p class="text-uppercase">
												<strong>Giá trị cốt lõi</strong>
											</p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
@endsection('main')
