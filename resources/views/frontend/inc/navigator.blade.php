<ul class="nav nav-pills" id="mainNav">

	<li class="activedropdown-full-color dropdown-secondary">
		<span class="glyphicon glyphicon-home"></span>
		<a href="/">
			Trang chủ
		</a>
	</li>
	<li class="dropdown-full-color dropdown-secondary">
		<a href="/gioi-thieu">
			Giới thiệu
		</a>
	</li>

	<li class="dropdown dropdown-full-color dropdown-secondary">
		<a class="dropdown-toggle">
			Bệnh nhân
		</a>
		<ul class="dropdown-menu">
			<li><a href="/dang-ky-kham">Đăng ký lịch khám</a></li>
			<li><a href="/dang-ky-thanh-vien">Đăng ký thành viên</a></li>
		</ul>
	</li>
	<li class="dropdown dropdown-full-color dropdown-secondary">
		<a class="dropdown-toggle">
			Bác sỹ
		</a>
		<ul class="dropdown-menu">
			<li><a href="/bac-sy/danh-sach">Danh sách bác sỹ</a></li>
			<li><a href="/bac-sy/dang-ky">Đăng ký làm thầy thuốc</a></li>
			<li><a href="/bac-sy/quy-dinh">Quy định</a></li>
		</ul>
	</li>

	<!-- <li class="dropdown dropdown-full-color dropdown-secondary">
		<a class="dropdown-toggle" href="/">
			Sản phẩm
		</a>
		<ul class="dropdown-menu">
			<li><a href="/dichvu">Các gói dịch vụ</a></li>
			<li><a href="/dungcuyte">Dụng cụ y tế</a></li>
			<li><a href="/thuoc">Sản phẩm bổ sung</a></li>
		</ul>
	</li> -->
	<!-- <li class="dropdown-full-color dropdown-secondary">
		<a href="/san-pham">
			Sản phẩm
		</a>
	</li> -->
	<li class="dropdown dropdown-full-color dropdown-secondary">
		<a class="dropdown-toggle">
			Tin tức
		</a>
		<ul class="dropdown-menu">
			<li><a href="/tin-tuc">Tin tức</a></li>
			<li><a href="/cam-nang">Cẩm nang sức khỏe</a></li>
			<li><a href="/tuyen-dung">Tuyển dụng</a></li>
		</ul>
	</li>
	<li class="dropdown dropdown-full-color dropdown-secondary">
		<a class="text-decoration-none" href="/khuyen-mai" >Khuyến mại</a>
	</li>
	<li class="dropdown-full-color dropdown-secondary">
		<a href="/lien-he">
			Liên hệ
		</a>
	</li>
</ul>
