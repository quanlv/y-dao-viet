<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>@yield('title')</title>



		<meta name="author" content="Nguyen Thanh Nga">
		<link rel="shortcut icon" href="/favicon.png">
		<meta name="description" content="Đang cập nhật">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/frontend/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/frontend/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/frontend/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="/frontend/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="/frontend/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="/frontend/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="/frontend/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/frontend/css/theme.css">
		<link rel="stylesheet" href="/frontend/css/theme-elements.css">
		<link rel="stylesheet" href="/frontend/css/theme-blog.css">
		<link rel="stylesheet" href="/frontend/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="/frontend/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="/frontend/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="/frontend/vendor/rs-plugin/css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/frontend/css/skins/skin-medical.css">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="/frontend/css/demos/demo-medical.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/frontend/css/custom.css">

		<!-- Head Libs -->
		<script src="/frontend/vendor/modernizr/modernizr.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106839876-1"></script>
		<script>
		  	window.dataLayer = window.dataLayer || [];
		  	function gtag(){dataLayer.push(arguments)};
		  	gtag('js', new Date());

		  	gtag('config', 'UA-106839876-1');
		</script>
