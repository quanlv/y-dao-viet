<div class="header-body">
					<div class="header-top header-top header-top-style-3 header-top-custom">
						<div class="container">
							<nav class="header-nav-top pull-right">
								<ul class="nav nav-pills">

									<li>
										<span class="ws-nowrap" style="color: red !important"><i class="icon-call-out icons" style="color: red !important"></i></i> 0936.326.158</span>
									</li>
									<li>
										<a href="" class="" data-toggle="modal" data-target="#registerMedicalApplication">
											Đăng ký khám
										</a>
									</li>
									<li>
									@if(Auth::user())
										<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											{{ Auth::user()->name }}
						                </a>
										<ul class="dropdown-menu dropdown-usermenu pull-right">
						                    <li>
						                        <a href="{{ route('logout') }}"
						                            onclick="event.preventDefault();
						                                     document.getElementById('logout-form').submit();">
						                            Logout
						                        </a>

						                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						                            {{ csrf_field() }}
						                        </form>
						                    </li>
					                  	</ul>

									@else
										<a href="" class="" data-toggle="modal" data-target="#myModal">
											Đăng nhập
										</a>
									@endif
									</li>

								</ul>
							</nav>
						</div>
					</div>

					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="/">
										<img alt="Porto" src="/images/logo-x1.png">
									</a>
								</div>
							</div>

							<div class="header-column">
								<div class="header-row">
									<div class="header-nav pt-xs">
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
											<i class="fa fa-bars"></i>
										</button>
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
											<nav>
												@include('frontend.inc.navigator')
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
