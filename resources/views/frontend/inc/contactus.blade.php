<section class="call-to-action call-to-action-default mb-none mt-xlg" style="background-color:rgb(244,244,244);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action-content">
								<h3>"Muốn <span style="color: red">CHẤM DỨT</span> cơn đau do thoái hoá cột sống <br> hãy đến với <span style="color: red">Y ĐẠO VIỆT</span>"</h3>
								<p>Liên hệ với chúng tôi để được tư vấn trực tiếp.</p>
							</div>
							<div class="call-to-action-btn">
								<a href="/lien-he" target="_blank" class="btn btn-lg btn-primary">Đăng ký khám</a>
							</div>
						</div>
					</div>
				</div>
</section>
