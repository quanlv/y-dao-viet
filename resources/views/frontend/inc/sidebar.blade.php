<aside class="sidebar">
								@if (!Auth::user())
								<div class="">
									<h5 class="heading-primary">Đăng nhập</h5>
									<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
						                        {{ csrf_field() }}

						                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						                            <label for="phone" class="control-label">Số điện thoại</label>

						                            <div class="">
						                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

						                                @if ($errors->has('phone'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('phone') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						                            <label for="password" class="control-label">Mật khẩu</label>

						                            <div class="">
						                                <input id="password" type="password" class="form-control" name="password" required>

						                                @if ($errors->has('password'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('password') }}</strong>
						                                    </span>
						                                @endif
						                            </div>
						                        </div>

						                        <div class="form-group">
						                            <div class="">
						                                <div class="checkbox">
						                                    <label>
						                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lưu đăng nhập
						                                    </label>
						                                </div>
						                            </div>
						                        </div>

						                        <div class="form-group">
						                            <div class="">
						                                <button type="submit" class="btn btn-primary">
						                                    Đăng nhập
						                                </button>

						                                <a class="btn btn-link" href="{{ route('password.request') }}">
						                                    Quên mật khẩu?
						                                </a>
						                            </div>
						                        </div>
						                    </form>
								</div>

								@endif
								

							</aside>
