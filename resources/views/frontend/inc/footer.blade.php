
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h4 class="mb-xlg">Địa chỉ</h4>
							<p>
								Công ty cổ phần Y Đạo Việt<br>
								Trạm y tế Trung Hoà, ngõ 35, Nguyễn Thị Định, Cầu Giấy, HN<br>
								Số điện thoại : 0936.326.158
							</p>
						</div>
						<div class="col-md-3">
							<h4 class="mb-xlg">Giờ hoạt động</h4>
							<div class="info custom-info">
								<span>Thứ 2 - Thứ 6 :</span>
								<span>8:30 - 17:00</span>
							</div>
							<div class="info custom-info">
								<span>Thứ 7, Chủ nhật : </span>
								<span>9:00 - 20:00</span>
							</div>
						</div>
						<div class="col-md-3 col-md-offset-1">
							<div class="contact-details">
								<h4 class="mb-xlg">Hotline</h4>
								<a class="text-decoration-none" href="/tel:1234567890">
									<h3>0963 901 284</h3>
								</a>
							</div>
						</div>
						<div class="col-md-2">
							<h4 class="mb-xlg">Cộng đồng</h4>
							<ul class="social-icons">
								<li class="social-icons-facebook">
									<a href="/http://www.facebook.com/" target="_blank" title="Facebook">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li class="social-icons-twitter">
									<a href="/http://www.twitter.com/" target="_blank" title="Twitter">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li class="social-icons-linkedin">
									<a href="/http://www.linkedin.com/" target="_blank" title="Linkedin">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright pt-md pb-md">
					<div class="container">
						<div class="row">
							<div class="col-md-12 center m-none">
								<p>©©©</p>
							</div>
						</div>
					</div>
				</div>
