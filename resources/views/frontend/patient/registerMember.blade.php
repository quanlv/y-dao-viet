@extends('frontend.layouts.master')
<head>
	<style >

	</style>
</head>
@section('title')
Đăng ký làm bác sỹ
@endsection
@section('main')
			<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Đăng ký làm thành viên <span>Vui lòng điền thông tin dưới đây</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Đăng ký làm thành viên</li>
							</ul>
						</div>
					</div>
				</div>
			</section>


			<div class="container" >
				<div class="col-md-9">
 				<div class="col-md-10"  >
					<div class="featured-box featured-box-primary align-left mt-xlg">
						<div class="box-content">
							<h4 class="heading-primary text-uppercase mb-md">Đăng ký thành viên</h4>
							<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
		                        {{ csrf_field() }}

		                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		                            <label for="name" class="col-md-4 control-label">Họ và tên</label>

		                            <div class="col-md-6">
		                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

		                                @if ($errors->has('name'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('name') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
		                            <label for="name" class="col-md-4 control-label">Số điện thoại</label>

		                            <div class="col-md-6">
		                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

		                                @if ($errors->has('phone'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('phone') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                            <label for="email" class="col-md-4 control-label">E-Mail</label>

		                            <div class="col-md-6">
		                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

		                                @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		                            <label for="password" class="col-md-4 control-label">Mật khẩu</label>

		                            <div class="col-md-6">
		                                <input id="password" type="password" class="form-control" name="password" required>

		                                @if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật khẩu</label>

		                            <div class="col-md-6">
		                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
		                            </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="col-md-6 col-md-offset-4">
		                                <button type="submit" class="btn btn-primary">
		                                    Register
		                                </button>
		                            </div>
		                        </div>
		                    </form>
							</div>
					</div>
				</div>
				</div>
				<div class="col-md-3">
				@include('frontend.inc.sidebar')
				</div>
			</div>
@include('frontend.inc.contactus')
@endsection()
