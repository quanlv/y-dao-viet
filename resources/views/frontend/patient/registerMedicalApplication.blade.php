@extends('frontend.layouts.master')
@section('title')
Đăng ký khám
@endsection
@section('main')
<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="mt-xs">Đặt lịch khám bệnh <span>Đăng ký đặt lịch khám bệnh với chúng tôi</span></h1>
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="#">Trang chủ</a></li>
					<li class="active">Đặt lịch khám</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<div class="container">
	<div class="col-md-9">
		<div class="col-md-10"  >
			@if(session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
			@endif
			<div class="featured-box featured-box-primary align-left mt-xlg">

				<div class="box-content">
					<h4 class="heading-primary text-uppercase mb-md">Đăng ký khám bệnh</h4>
					<form action="/dang-ky-kham" id="frmSignUp" method="post">
						{{ csrf_field() }}
							<div class="form-group">

									<label>Họ tên</label>
									<input type="text" name="name" class="form-control input-lg">

							</div>
							<div class="form-group">

									<label>Số điện thoại</label>
									<input type="text" name="telephone" class="form-control input-lg">

							</div>
							<div class="form-group">
								<label for="sel1">Gói dịch vụ</label>
								<select class="form-control input-lg" name="pack"id="sel1">
									<option disabled="" selected="" />
									<option value="10"> Gói 10 buổi</option>
									<option value="5"> Gói 5 buổi</option>
									<option value="3"> Gói 3 buổi</option>
									<option value="0"> Dùng thử</option>
								</select>

							</div>


							<div class="form-group">

								<button type="submit" class="btn btn-success btn-block btn-lg" >
									Đăng ký
								</button>

							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		@include('frontend.inc.sidebar')
	</div>
</div>

@include('frontend.inc.contactus')
@endsection()
