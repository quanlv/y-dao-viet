@extends('frontend.layouts.master')
<head>
	<style >

	</style>
</head>
@section('title')
Đăng ký làm bác sỹ
@endsection
@section('main')
			<section class="page-header page-header-color page-header-primary page-header-float-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="mt-xs">Đăng ký làm bác sỹ <span>Vui lòng điền thông tin dưới đây</span></h1>
							<ul class="breadcrumb breadcrumb-valign-mid">
								<li><a href="#">Trang chủ</a></li>
								<li class="active">Đăng ký làm bác sỹ</li>
							</ul>
						</div>
					</div>
				</div>
			</section>


			<div class="container" >
				<div class="col-md-9">
					@if(session('message'))
					<div class="alert alert-success">
						{{ session('message') }}
					</div>
					@endif
					<div class="featured-box featured-box-primary align-left mt-xlg">
							<div class="box-content">
								<h4 class="heading-primary text-uppercase mb-md">Đăng ký làm bác sỹ</h4>
							<form action="{{route('dang-ky-bac-sy')}}" id="frmSignUp" method="post">
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Họ tên</label>
											<input name="fullname" type="text" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Số điện thoại</label>
											<input name="telephone" type="text" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Email</label>
											<input name="email" type="text" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Địa chỉ liên hệ</label>
											<input name="address" type="text" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Khu vực làm việc</label>
											<input name="work_address" type="text" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Mật khẩu</label>
											<input name="password" type="password" value="" class="form-control input-lg">
										</div>
									</div>
								</div>
									<div class="row">
										<div class="col-md-12">
										<input type="submit" value="Đăng ký" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
										</div>
									</div>
							</form>
							</div>
					</div>
				</div>
			</div>
@include('frontend.inc.contactus')
@endsection()
