@extends('admin.layouts.apps')
@section('title')
Thêm người dùng
@endsection
@section('main')

<div class="col-md-12 col-sm-12 col-xs-12">
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade in" user="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Lỗi!</strong>

        <ul>
            @foreach ($errors->all() as $message)
            <li>{{$message}}</li>
            @endforeach
        </ul>

    </div>
    @endif
    <div class="x_panel">
        <div class="x_title">
            <h2>Thêm người dùng</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form action="{{ route('admin.user.store') }}" id="add_user" data-parsley-validate class="form-horizontal form-label-left" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên người dùng <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="name" type="text" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="email" type="text" class="form-control col-md-7 col-xs-12" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="password" type="password"  class="form-control col-md-7 col-xs-12" required="required">
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password Confirmation <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="password_confirmation" type="password"  class="form-control col-md-7 col-xs-12" required="required">
                    </div>
                </div>

                <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Phân quyền</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    @foreach($roles as $role)
                    <div class="checkbox">
                        <label>
                          <input type="checkbox" name="{{$role->id}}" value="{{$role->name}}" >
                          {{ $role->display_name?$role->display_name:$role->name }}
                        </label>
                      </div>
                      @endforeach
                </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                        <a href=".">
                            <button class="btn btn-default" type="button">Quay lại</button>
                        </a>
                        <button class="btn btn-default" type="reset">Reset</button>
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Parsley -->
<script src="{{ asset('backend/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
@endsection
