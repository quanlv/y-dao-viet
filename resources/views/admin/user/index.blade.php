@extends('admin.layouts.apps')
@section('title')
Danh sách thành viên
@endsection
@section('main')
<div class="col-md-12 col-sm-12 col-xs-12">

	@if(Session::has('flash_message'))
	<div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
	@endif
	<div class="x_panel">
		<div class="x_title">
			<h2>Danh sách người dùng</h2>                    
			<div class="clearfix"></div>
		</div>

		<div class="x_content">
			<div class="table-responsive">
			<?php echo $users->render(); ?>
				<table class="table table-striped jambo_table bulk_action">
					<thead>
						<tr class="headings">
							<td>ID</td>
							<td>Tên</td>
							<td>Email</td>
							<td>Phân vai</td>
							<!-- <td>Xem</td> -->
							<td>Sửa</td>
							<td>Xóa</td>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>
								@foreach($roles as $role)
									@if($user->hasRole($role->name))
										<a href="{{ route('admin.role.edit',['role' => $role]) }}" target="_blank">
											{{ $role->display_name?$role->display_name:$role->name }}
										</a>
										<br/>
									@endif
								@endforeach
							</td>
							<!-- <td>
								<a href="{{ route('admin.user.show',['user' => $user]) }}">Xem</a>
							</td> -->
							<td>
								<a href="{{ route('admin.user.edit',['user' => $user]) }}">Sửa</a>
							</td>
							<td>													
								<a href="#" class="remove_link text-danger" data-toggle="modal" data-target="#destroy-user" data-value="{{ route('admin.user.destroy',['user' => $user]) }}">Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<?php echo $users->render(); ?>
			</div>
		</div>
	</div>
</div>
<div id="destroy-user" class="modal fade bs-example-modal-lg" tabindex="-1" user="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Xóa người dùng</h4>
			</div>
			<div class="modal-body">
				<p>Bạn có chắc chắn xóa người dùng này chứ?</p>
			</div>
			<div class="modal-footer">
				<button id="close_button" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<form id="destroy_form" style="display:inline;" action="" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					
					<button type="submit" class="btn btn-danger">Xóa</button>
				</form>				
			</div>

		</div>
	</div>
</div>

@endsection
@section('script')
<script>
	$(document).ready(function(){
		$(".remove_link").click(function(){
			$('#destroy_form').attr("action",$(this).attr("data-value"));
			
		});
		$("#close_button").click(function(){
			$('#destroy_form').attr("action",'');
		});
	});
</script>
@endsection