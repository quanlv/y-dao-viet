<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Quản lý nội dung</h3>
                <ul class="nav side-menu">
                  <li><a href="{{ route('admin.index') }}"><i class="fa fa-home"></i> Bảng tin </a>
                  </li>
                  <li><a href="#"><i class="fa fa-edit"></i> Bài viết <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('admin.post.create') }}">Viết bài mới</a></li>
                      <li><a href="{{ route('admin.post.index') }}">Danh sách bài viết</a></li>
                      <li><a href="{{ route('admin.category.index') }}">Danh mục</a></li>
                      <li><a href="{{ route('admin.tag.index') }}">Thẻ</a></li>
                    </ul>
                  </li>                  
                  <li><a><i class="fa fa-files-o"></i> Trang <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Tạo trang mới</a></li>
                      <li><a href="#">Danh sách trang</a></li>
                    </ul>
                  </li>                  
                </ul>
              </div>
              <div class="menu_section">
                <h3>Quản lý người dùng</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-envelope"></i> Liên hệ </a>
                  </li>

                  <li><a><i class="fa fa-user"></i> Thành viên <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('admin.user.create') }}">Thêm thành viên</a></li>
                      <li><a href="{{ route('admin.user.index') }}">Danh sách thành viên</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-users"></i> Vai <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('admin.role.create') }}">Thêm vai</a></li>
                      <li><a href="{{ route('admin.role.index') }}">Danh sách vai trò</a></li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-users"></i> Phân quyền <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('admin.permission.create') }}">Thêm quyền</a></li>
                      <li><a href="{{ route('admin.permission.index') }}">Danh sách quyền</a></li>
                      <li><a href="{{ route('admin.permission.install') }}">Khởi tạo quyền <span class="label label-danger pull-right">Nguy hiểm</span></a></li>
                    </ul>
                  </li>

                </ul>
              </div>

              <div class="menu_section">
                <h3>Cài đặt</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-cogs"></i> Cài đặt <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Cấu hình website</a></li>
                      <li><a href="#">Chèn mã html</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

              <!-- <div class="menu_section">
                <h3>Quản lý căn hộ</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Căn hộ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Thêm căn hộ</a></li>
                      <li><a href="#">Danh sách căn hộ</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-building"></i> Dự án <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Thêm dự án</a></li>
                      <li><a href="#">Danh sách dự án</a></li>
                    </ul>
                  </li>
                </ul>
              </div> -->

            </div>