@extends('admin.layouts.apps')
@section('title')
Danh sách thành viên
@endsection
@section('main')
<div class="col-md-12 col-sm-12 col-xs-12">

	@if(Session::has('flash_message'))
	<div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
	@endif
	<div class="x_panel">
		<button data-toggle="modal" data-target="#create-category" class="btn btn-primary">Thêm danh mục</button>
	</div>
	<div class="x_panel">
		<div class="x_title">
			<h2>Danh sách danh mục</h2>                    
			<div class="clearfix"></div>
		</div>

		<div class="x_content">
			<div class="table-responsive">			
				<table class="table table-striped jambo_table bulk_action">
					<thead>
						<tr class="headings">
							<td>ID</td>
							<td>Tên</td>
							<td>Đường dẫn</td>
							<td>Miêu tả</td>
							<!-- <td>Xem</td> -->
							<td>Sửa</td>
							<td>Xóa</td>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
						<tr>
							<td>{{ $category->id }}</td>
							<td>{{ $category->name }}</td>
							<td>{{ $category->slug }}</td>
							<td>{{ $category->description }}</td>
							<!-- <td>
								<a href="{{ route('admin.category.show',['category' => $category]) }}">Xem</a>
							</td> -->
							<td>
								<a href="{{ route('admin.category.edit',['category' => $category]) }}">Sửa</a>
							</td>
							<td>													
								<a href="#" class="remove_link text-danger" data-toggle="modal" data-target="#destroy-category" data-value="{{ route('admin.category.destroy',['category' => $category]) }}">Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="destroy-category" class="modal fade bs-example-modal-lg" tabindex="-1" category="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Xóa danh mục</h4>
			</div>
			<div class="modal-body">
				<p>Bạn có chắc chắn xóa danh mục này chứ?</p>
			</div>
			<div class="modal-footer">
				<button id="close_button" type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
				<form id="destroy_form" style="display:inline;" action="" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					
					<button type="submit" class="btn btn-danger">Xóa</button>
				</form>				
			</div>

		</div>
	</div>
</div>
<div id="create-category" class="modal fade bs-example-modal-lg" tabindex="-1" category="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Thêm danh mục</h4>
			</div>
			<div class="modal-body">
				<form id="create_form" style="display:inline;" action="{{ route('admin.category.store') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Tên danh mục</label>
						<input class="form-control" type="text" name="name" required="" placeholder="Tên danh mục*">
					</div>
					<div class="form-group">
						<label>Đường dẫn</label>
						<input type="text" name="slug" placeholder="Đường dẫn" class="form-control">
					</div>
					<div class="form-group">
						<label>Miêu tả</label>
						<textarea placeholder="Miêu tả" name="description" class="form-control"></textarea>						
					</div>
					<div class="form-group">
						<label>Danh mục</label>
						<select class="form-control" name="parent_category_id">
							<option></option>
							@foreach ($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
					</div>
					
					
					
				
			</div>
			<div class="modal-footer">
					<button id="close_button" type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>	
					<button type="submit" class="btn btn-primary">Thêm</button>
				</form>				
			</div>

		</div>
	</div>
</div>

@endsection
@section('script')
<script>
	$(document).ready(function(){
		$(".remove_link").click(function(){
			$('#destroy_form').attr("action",$(this).attr("data-value"));
			
		});
		$("#close_button").click(function(){
			$('#destroy_form').attr("action",'');
		});
	});
</script>
@endsection