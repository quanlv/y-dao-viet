@extends('admin.layouts.apps')
@section('title')
Thêm quyền
@endsection
@section('main')    

<div class="col-md-12 col-sm-12 col-xs-12">
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Lỗi!</strong> 
        <ul>
            @foreach ($errors->all() as $message) 
            <li>{{$message}}</li>
            @endforeach
        </ul>

    </div>
    @endif
    <div class="x_panel">
        <div class="x_title">
            <h2>Thêm quyền</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form action="{{ route('admin.permission.store') }}" id="add_permission"  class="form-horizontal form-label-left" method="POST">
                {{ csrf_field() }}

                <div class="form-group ">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên quyền <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="name" type="text" class="form-control col-md-7 col-xs-12 {{ ($errors->has('name'))?'parsley-error':'' }}" required="" >
                        @if ($errors->has('name'))
                        <ul class="parsley-errors-list">
                            @foreach ($errors->get('name') as $message) 
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                    
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên hiển thị quyền
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="display_name" type="text" class="form-control col-md-7 col-xs-12 {{ ($errors->has('display_name'))?'parsley-error':'' }}">
                        @if ($errors->has('display_name'))
                        <ul class="parsley-errors-list">
                            @foreach ($errors->get('name') as $message) 
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Miêu tả
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="description" type="text"  class="form-control col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                        <a href=".">
                            <button class="btn btn-default" type="button">Quay lại</button>
                        </a>
                        <button class="btn btn-default" type="reset">Reset</button>
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@section('script')

@endsection