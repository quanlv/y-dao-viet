@extends('admin.layouts.apps')
@section('title')
Sửa quyền
@endsection
@section('main')    

<div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('flash_message'))
        <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
    @endif
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Lỗi!</strong> 

        <ul>
            @foreach ($errors->all() as $message) 
            <li>{{$message}}</li>
            @endforeach
        </ul>

    </div>
    @endif
    <div class="x_panel">
        <div class="x_title">
            <h2>Sửa quyền</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form action="{{ route('admin.permission.update',['permission' => $permission]) }}" id="add_permission" data-parsley-validate class="form-horizontal form-label-left" method="POST">

                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên quyền <span class="required">
                    </span>
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="name" type="text" disabled class="form-control col-md-7 col-xs-12" value="{{$permission->name}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên hiển thị quyền
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="display_name" type="text" class="form-control col-md-7 col-xs-12" value="{{$permission->display_name}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Miêu tả
                    </label>
                    <div class="col-md-6 col-sm-9 col-xs-12">
                        <input name="description" type="text"  class="form-control col-md-7 col-xs-12" value="{{$permission->description}}">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                        <a href="..">
                            <button class="btn btn-default" type="button">Quay lại</button>
                        </a>
                        <button class="btn btn-default" type="reset">Reset</button>

                        <button type="submit" class="btn btn-success">Cập nhật</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<!-- Parsley -->
<script src="{{ asset('backend/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
@endsection