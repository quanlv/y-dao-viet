@extends('admin.layouts.apps')
@section('title')
Danh sách thành viên
@endsection
@section('main')
	<p>Sau đây là danh sách các quyền sẽ được cài đặt</p>
	<ul>
		@foreach($permissions as $permission)
			<li>
				{{ $permission }}
			</li>
		@endforeach
	</ul>
	<p class="text-danger">Việc cài đặt này có thể làm mất hết danh sách quyền!</p>
	<form action="{{route('admin.permission.install')}}" method="post">
		{{ csrf_field() }}
		<button type="submit">Cài đặt</button>		
	</form>
@endsection
@section('script')

@endsection