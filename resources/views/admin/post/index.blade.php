@extends('admin.layouts.apps')
@section('title')
Danh sách thành viên
@endsection
@section('main')
<div class="col-md-12 col-sm-12 col-xs-12">

	@if(Session::has('flash_message'))
	<div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
	@endif
	<div class="x_panel">
		<div class="x_title">
			<h2>Danh sách bài viết</h2>
			<div class="clearfix"></div>
		</div>

		<div class="x_content">
			<div class="table-responsive">
			<?php echo $posts->render(); ?>
				<table class="table table-striped jambo_table bulk_action">
					<thead>
						<tr class="headings">
							<td>ID</td>
							<td>Tên bài viết</td>
							<td>Sửa</td>
							<td>Xóa</td>
						</tr>
					</thead>
					<tbody>
						@foreach($posts as $post)
						<tr>
							<td>{{ $post->id }}</td>
							<td><a href="{{ $post->getUrl() }}" target="_blank">{{ $post->title }}</a></td>

							<!-- <td>
								<a href="{{ route('admin.post.show',['post' => $post]) }}">Xem</a>
							</td> -->
							<td>
								<a href="{{ route('admin.post.edit',['post' => $post]) }}">Sửa</a>
							</td>
							<td>
								<a href="#" class="remove_link text-danger" data-toggle="modal" data-target="#destroy-post" data-value="{{ route('admin.post.destroy',['post' => $post]) }}">Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<?php echo $posts->render(); ?>
			</div>
		</div>
	</div>
</div>
<div id="destroy-post" class="modal fade bs-example-modal-lg" tabindex="-1" post="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Xóa bài viết</h4>
			</div>
			<div class="modal-body">
				<p>Bạn có chắc chắn xóa bài viết này chứ?</p>
			</div>
			<div class="modal-footer">
				<button id="close_button" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<form id="destroy_form" style="display:inline;" action="" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}

					<button type="submit" class="btn btn-danger">Xóa</button>
				</form>
			</div>

		</div>
	</div>
</div>

@endsection
@section('script')
<script>
	$(document).ready(function(){
		$(".remove_link").click(function(){
			$('#destroy_form').attr("action",$(this).attr("data-value"));

		});
		$("#close_button").click(function(){
			$('#destroy_form').attr("action",'');
		});
	});
</script>
@endsection
