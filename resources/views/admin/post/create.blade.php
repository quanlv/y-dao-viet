@extends('admin.layouts.apps')
@section('title')
Thêm bài viết
@endsection
@section('main')

<div class="col-md-12 col-sm-12 col-xs-12">
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong>Lỗi!</strong>
        <ul>
            @foreach ($errors->all() as $message)
            <li>{{$message}}</li>
            @endforeach
        </ul>

    </div>
    @endif
    <div class="x_panel">
        <div class="x_title">
            <h2>Thêm bài viết</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form action="{{ route('admin.post.store') }}" id="add_post"  class="form-horizontal form-label-left" method="POST">
                {{ csrf_field() }}

                <div class="input-group">
                   <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="avatar">
                </div>
                <div class="form-group">
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                </div>


                <div class="form-group ">
                    <label>Tiêu đề bài viết<span class="required">*</span>
                    </label>

                        <input name="title" type="text" class="form-control col-md-7 col-xs-12 {{ ($errors->has('title'))?'parsley-error':'' }}" required="" >
                        @if ($errors->has('title'))
                        <ul class="parsley-errors-list">
                            @foreach ($errors->get('title') as $message)
                            <li>{{$message}}</li>
                            @endforeach
                        </ul>
                        @endif


                </div>
                <div class="form-group">
                    <label>Nội dung</label>
                    <textarea id="my-editor" name="content" class="form-control">{!! old('content', '') !!}</textarea>
                </div>

               <div class="form-group">
                   <label>Seo title</label>
                   <input type="text" name="seo_title" class="form-control">
               </div>

               <div class="form-group">
                   <label>Seo description</label>
                   <input type="text" name="seo_description" class="form-control">
               </div>

               <div class="form-group">
                    <label>Danh mục</label>
                    @foreach($categories as $category)
                    <div class="checkbox">
                        <label><input type="checkbox" name="category[]" value="{{ $category->id }}">{{ $category->name }}</label>
                    </div>
                    @endforeach
               </div>

               <div class="form-group">
                    <label for="">Đăng nháp</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="deleted_at" value="{{ date("Y-m-d H:i:s") }}">Đăng nháp</label>
                    </div>
               </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                        <a href=".">
                            <button class="btn btn-default" type="button">Quay lại</button>
                        </a>
                        <button class="btn btn-default" type="reset">Reset</button>
                        <button type="submit" class="btn btn-success">Thêm</button>

                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
  };

</script>

<script>
    $ckedit = CKEDITOR.replace('my-editor', options);
    $ckedit.config.extraPlugins = 'format';
    $('#lfm').filemanager('image');
    $('#lfm').filemanager('file');

</script>




@endsection
